package net.infobosccoma.coloredconsole;

/**
 * Tipus enumarat que conté els diferents tipus d'avís
 * 
 * @author marc
 */
public enum TipusMissatge {
    INFORMATIU, AVIS, ERROR
}

