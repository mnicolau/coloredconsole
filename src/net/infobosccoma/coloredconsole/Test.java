
package net.infobosccoma.coloredconsole;

import static net.infobosccoma.coloredconsole.Basics.*;
import org.fusesource.jansi.Ansi.Color;

/**
 * Programa d'exemple d'utilització de la llibreria
 * 
 * @author marc
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        esborrarPantalla();
//        escriureTitol(Color.BLACK, Color.CYAN, "T I T O L  D E L  F O R M U L A R I", 1);
//        escriureText(Color.WHITE, Color.BLACK, "Codi :", 5, 5);
//        caixaText(12, 5, 10);
//        escriureText(Color.WHITE, Color.BLACK, "Nom  :", 5, 6);
//        caixaText(12, 6, 10);
//        
//        String h = llegirText(13, 5);
//        float i = llegirReal(13, 6);

        Menu menu = new Menu();
        menu.afegirOpcio("Entrar un client", 1);
        menu.afegirOpcio("Afegir client", 2);
        menu.afegirOpcio("Esborrar un client", 3);
        menu.afegirOpcio("Mostrar", 4);
        menu.afegirOpcio("Sortir", 5);

        int opcio;
        do {
            esborrarPantalla();
            menu.mostrar();
            switch (opcio = menu.llegirOpcio(30, 16)) {
                case 1:
                    opcio1();
                    break;
                case 2:
                    System.out.println(2);
                    break;
                case 3:
                    System.out.println(3);
                    break;
                case 4:
                    System.out.println(4);
                    break;
                case 5:
                    System.out.println(5);
                    break;
            }
        } while (opcio != 5);
        pausa();
    }

    static void opcio1() {
        esborrarPantalla();
        escriureTitol(Color.BLACK, Color.CYAN, "Opcio 1", 1);
        escriureText(Color.WHITE, Color.BLACK, "Codi :", 5, 5);
        escriureText(Color.BLACK, Color.WHITE, "12", 12, 5);
        llegirEnter(12, 5);

//        escriureText(Color.WHITE, Color.BLACK, "Nom  :", 5, 6);
//        caixaText(12, 6, 10);
        //pausa();
    }
}
