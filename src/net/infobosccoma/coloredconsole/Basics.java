package net.infobosccoma.coloredconsole;

import java.util.Scanner;
import static net.infobosccoma.coloredconsole.Basics.escriureText;
import static net.infobosccoma.coloredconsole.Basics.llegirText;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.Ansi.Color;
import org.fusesource.jansi.Ansi.Erase;
import static org.fusesource.jansi.Ansi.ansi;
import org.fusesource.jansi.AnsiConsole;

/**
 * Mòdul que conte subprogrames bàsics per donar format a la pantalla.
 *
 * @author marc
 */
public class Basics {

    private static Ansi.Color colors;

    /**
     * Esborra tota la consola
     */
    public static void esborrarPantalla() {
        AnsiConsole.systemInstall();
        Ansi pantalla = ansi().eraseScreen().cursor(1, 1);
        pantalla = pantalla.reset();
        System.out.print(pantalla);
        AnsiConsole.systemUninstall();
    }

    /**
     * Esborra tota la línia on hi ha situat el cursor
     */
    public static void esborrarLinia() {
        AnsiConsole.systemInstall();
        Ansi pantalla = ansi().eraseLine(Erase.ALL);
        pantalla = pantalla.reset();
        System.out.print(pantalla);
        AnsiConsole.systemUninstall();
    }

    /**
     * Situa el cursor a uan posició de la consola.
     *
     * @param x la coordenada horitzontal on es vol situar el cursor
     * @param y la coordenada vertical on es vol situar el cursor
     */
    public static void situarCursor(int x, int y) {
        Ansi pantalla;
        AnsiConsole.systemInstall();
        pantalla = ansi().cursor(y, x);
        System.out.print(pantalla.reset());
        AnsiConsole.systemUninstall();
    }

    /**
     * Escriu un text per la consola. Per defecte el color del text és blanc i
     * el del fons és negre.
     *
     * @param text El text que es vol mostrar.
     */
    public static void escriureText(String text) {
        escriureText(Color.WHITE, Color.BLACK, text);
    }

    /**
     * Escriu un text per la consola en unes coordenades indicades. Per defecte
     * el color del text és blanc i el del fons és negre.
     *
     * @param text El text que es vol mostrar.
     * @param x La posició x (columna dins una fila) on es vol mostrar.
     * @param y La posició y (fila dins una columna) on es vol mostrar.
     */
    public static void escriureText(String text, int x, int y) {
        escriureText(Color.WHITE, Color.BLACK, text, x, y);
    }

    /**
     * Escriu un text per la consola amb el color de text i de fons indicats.
     *
     * @param colorText El color del text amb el qual es vol mostrar.
     * @param colorFons El color de fons amb el qual es vol mostrar.
     * @param text El text que es vol mostrar.
     */
    public static void escriureText(Color colorText, Color colorFons, String text) {
        Ansi pantalla;
        AnsiConsole.systemInstall();
        pantalla = ansi().fg(colorText).bg(colorFons);
        pantalla = pantalla.a(text);
        System.out.print(pantalla.reset());
        AnsiConsole.systemUninstall();
    }

    /**
     * Escriu un text per la consola amb el color de text i de fons indicats.
     *
     * @param colorText El color del text amb el qual es vol mostrar.
     * @param colorFons El color de fons amb el qual es vol mostrar.
     * @param text El text que es vol mostrar.
     * @param x La posició x (columna dins una fila) on es vol mostrar.
     * @param y La posició y (fila dins una columna) on es vol mostrar.
     */
    public static void escriureText(Color colorText, Color colorFons, String text, int x, int y) {
        Ansi pantalla;
        AnsiConsole.systemInstall();
        pantalla = ansi().cursor(y, x).fg(colorText).bg(colorFons);
        pantalla = pantalla.a(text);
        System.out.print(pantalla.reset());
        AnsiConsole.systemUninstall();
    }

    /**
     * Escriu un títol (banda de color amb text) en una posició de la consola i
     * amb uns colors
     *
     * @param colorText color del text
     * @param colorFons color de fons
     * @param text text que s'ha de mostrar
     * @param y a quina línia de la consola s'ha de mostrar el títol
     */
    public static void escriureTitol(Color colorText, Color colorFons, String text, int y) {
        escriureText(colorText, colorFons, String.format("%80s", " "), 0, y);
        text = generarTextCentrat(text);
        escriureText(colorText, colorFons, text, 0, y + 1);
        escriureText(colorText, colorFons, String.format("%80s", " "), 0, y + 2);

        System.out.println();
    }

    /**
     * Escriu un missatge d'un tipus concret, que pot ser AVIS, INFORMATIU o
     * ERROR
     *
     * @param tipus el tipus de missatge a mostrar
     * @param text el text a mostrar
     */
    public static void escriureMissatge(TipusMissatge tipus, String text) {
        switch (tipus) {
            case AVIS:
                escriureTitol(Color.BLACK, Color.YELLOW, "A V I S", 9);
                escriureTitol(Color.BLACK, Color.YELLOW, text, 12);
                pausa();
                break;
            case INFORMATIU:
                escriureTitol(Color.BLACK, Color.CYAN, "I N F O R M A C I O", 9);
                escriureTitol(Color.BLACK, Color.CYAN, text, 12);
                pausa();
                break;
            case ERROR:
                escriureTitol(Color.WHITE, Color.RED, "E R R O R", 9);
                escriureTitol(Color.WHITE, Color.RED, text, 12);
                pausa();
                break;
        }
    }

    /**
     * Mostra una franja d'una mida concreta en unes coordenades de la consola,
     * amb color de fons blanc.
     *
     * @param x la coordenada horitzontal
     * @param y la coordenada vertical
     * @param mida la mida que ha de tenir
     */
    public static void caixaText(int x, int y, int mida) {
        escriureText(Color.BLACK, Color.WHITE, String.format("%" + mida + "s", " "), x, y);
        situarCursor(x, y);
    }

    /**
     * Llegeix un text de la consola, situant el cursor a una posició x,y
     *
     * @param x la coordenada horitzontal
     * @param y la coordenada vertical
     * @return el text que s'ha llegit
     */
    public static String llegirText(int x, int y) {
        Ansi pantalla;
        AnsiConsole.systemInstall();
        pantalla = ansi().cursor(y, x).fg(Color.BLACK).bg(Color.WHITE);
        System.out.print(pantalla);
        String result = new Scanner(System.in).nextLine().trim();
        System.out.print(pantalla.reset());
        AnsiConsole.systemUninstall();
        return result;
    }

    /**
     * Llegeix un text de la consola, situant el cursor a una posició x,y i amb
     * una mida de caixa
     *
     * @param x la coordenada horitzontal
     * @param y la coordenada vertical
     * @param mida la mida de la caixa
     * @return el text que s'ha llegit
     */
    public static String llegirTextCaixa(int x, int y, int mida) {
        Ansi pantalla;
        AnsiConsole.systemInstall();
        pantalla = ansi().cursor(y, x).fg(Color.BLACK).bg(Color.WHITE);
        pantalla = pantalla.a(String.format("%" + mida + "s", " "));
        pantalla = pantalla.cursor(y, x);
        System.out.print(pantalla);
        String result = new Scanner(System.in).nextLine();
        System.out.print(pantalla.reset());
        AnsiConsole.systemUninstall();
        return result;
    }

    /**
     * Llegeix un enter de la consola, situant el cursor a una posició x,y.
     * Controla qualsevol error de lectura que es pugui produir. En cas d'error
     * retorna un 0.
     *
     * @param x la coordenada horitzontal
     * @param y la coordenada vertical
     * @return l'enter que s'ha llegit.
     */
    public static int llegirEnter(int x, int y) {
        int result = 0;
        try {
            result = Integer.parseInt(llegirText(x, y));
        } catch (Exception e) {

        }
        return result;
    }

    /**
     * Llegeix un enter de la consola, situant el cursor a una posició x,y.
     * Controla qualsevol error de lectura que es pugui produir. En cas d'error
     * retorna un 0.
     *
     * @param x la coordenada horitzontal
     * @param y la coordenada vertical
     * @param mida la mida de la caixa
     *
     * @return l'enter que s'ha llegit.
     */
    public static int llegirEnterCaixa(int x, int y, int mida) {
        int result = 0;
        try {
            result = Integer.parseInt(llegirTextCaixa(x, y, mida));
        } catch (Exception e) {

        }
        return result;
    }
    
    /**
     * Llegeix un real de la consola, situant el cursor a una posició x,y.
     * Controla qualsevol error de lectura que es pugui produir. En cas d'error
     * retorna un 0.
     *
     * @param x la coordenada horitzontal
     * @param y la coordenada vertical
     * @return l'enter que s'ha llegit.
     */
    public static float llegirReal(int x, int y) {
        float result = 0;
        try {
            result = Float.parseFloat(llegirText(x, y));
        } catch (Exception e) {

        }
        return result;
    }

    /**
     * Fa una pausa, mostrant un text d'espera i esperant que es premi RETORN.
     */
    public static void pausa() {
        System.out.println("\n");
        escriureText(Color.WHITE, Color.BLACK, generarTextCentrat("<<Prem qualsevol tecla per continuar>>"));
        new Scanner(System.in).nextLine();
    }

    /**
     * Formata un text centrat horitzontalment.
     *
     * @param text El text que es vol formatar.
     * @return El text ja formatat (centrat).
     */
    private static String generarTextCentrat(String text) {
        int llarg = text.length();
        int pos = 40 - text.length() / 2;
        int residu = (pos * 2 + llarg) % 2;
        return String.format("%" + pos + "s%s%" + (residu == 0 ? pos : pos - 1) + "s", " ", text, " ");
    }

    /**
     * Obtenir la variable que indica quins colors hi ha disponibles.
     * 
     * @return Els colors disponibles.
     */
    public static Color getColors() {
        return colors;
    }
}
