
package net.infobosccoma.coloredconsole;


import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import static net.infobosccoma.coloredconsole.Basics.esborrarLinia;
import static net.infobosccoma.coloredconsole.Basics.escriureText;
import static net.infobosccoma.coloredconsole.Basics.escriureTitol;
import static net.infobosccoma.coloredconsole.Basics.llegirEnter;
import org.fusesource.jansi.Ansi.Color;

/**
 * Mòdul que permet crear i gestionar un menú d'opcions.
 * 
 * @author marc
 */
public class Menu {
    private final String ESCULL_OPCIO_STR = "Escull una opcio:";
    private Map<Integer, String> opcions;

    /**
     * Creació d'un nou menú
     */
    public Menu() {
        opcions = new TreeMap<>();
    }

    /**
     * afegir una opció al menú
     * 
     * @param text El text que es vol mostrar pel menú
     * @param valorOpcio un enter major o igual a 1
     */
    public void afegirOpcio(String text, int valorOpcio) {
        if(valorOpcio < 1) {
            throw new IllegalArgumentException(String.format("El valor %d no està permès com a opció. Ha de ser superior o igual a 1", valorOpcio));
        }
        opcions.put(valorOpcio, text);
    }

    /**
     * Llegeix una opcio del menú, situant el cursor a una coordenada dins la consola
     * 
     * @param x la coordenada horitzontal
     * @param y la coordenada vertical
     * @return l'opció llegida
     */
    public int llegirOpcio(int x, int y) {
        int opcio;
        Set<Integer> valorOpcions = opcions.keySet();
        do {
            escriureText(Color.WHITE, Color.BLACK, "", x, y);
            esborrarLinia();
            escriureText(Color.WHITE, Color.BLACK, ESCULL_OPCIO_STR, x, y);
            opcio = llegirEnter(x + ESCULL_OPCIO_STR.length() + 1, y);
        } while (opcio < Collections.min(valorOpcions) || opcio > Collections.max(valorOpcions));
        return opcio;
    }
    
    /**
     * Mostra el menú d'opcions
     */
    public void mostrar() {
        int y = 5;
        escriureTitol(Color.BLACK, Color.CYAN, "M E N U  D ' O P C I O N S", y);
        y +=4;
        for (int opcio : opcions.keySet()) {
            escriureText(Color.WHITE, Color.BLACK, String.format("%d. %s", opcio, opcions.get(opcio)), 30, y++);
        }
    }
}
